<?php

Class Dashboard extends CI_Controller {
    
    function __construct(){
        parent::__construct();
    }

    function index(){
        $urlidn              = "https://api.kawalcorona.com/indonesia/provinsi";
        $urlworld            = "https://api.kawalcorona.com/";

        $geturlidn           = file_get_contents($urlidn);
        $get_urlworld        = file_get_contents($urlworld);

        $data['resultidn']   = json_decode($geturlidn);
        $data['resultworld'] = json_decode($get_urlworld);

        $this->load->view('dashboard_v',$data);
    }
}