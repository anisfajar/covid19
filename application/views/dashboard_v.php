<!DOCTYPE html>
<html lang="en">
<head>
<link rel="icon" href="<?php echo base_url() ?>public/img/code.png" type="image/png" >
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="600" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Data Covid-19</title>
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?php echo base_url()?>public/asset/plugins/fontawesome-free/css/all.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo base_url()?>public/asset/plugins/dropify/css/dropify.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>public/asset/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url()?>public/asset/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>public/asset/plugins/datatables-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="<?php echo base_url()?>public/asset/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>public/asset/plugins/swal/sweetalert2.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url()?>public/asset/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="<?php echo base_url()?>public/asset/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <script src="<?php echo base_url()?>public/asset/plugins/jquery/jquery.min.js"></script>
</head>
<body>
    <script src="<?php echo base_url()?>public/asset/plugins/dropify/js/dropify.min.js"></script>
    <script src="<?php echo base_url()?>public/asset/plugins/select2/js/select2.full.min.js"></script>
    <script src="<?php echo base_url()?>public/asset/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo base_url()?>public/asset/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url()?>public/asset/dist/js/adminlte.js"></script>

    <!-- OPTIONAL SCRIPTS -->
    <script src="<?php echo base_url();?>public/asset/dist/js/demo.js"></script>

    <!-- PAGE PLUGINS -->
    <!-- jQuery Mapael -->
    <script src="<?php echo base_url()?>public/asset/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
    <script src="<?php echo base_url()?>public/asset/plugins/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url()?>public/asset/plugins/jquery-mapael/jquery.mapael.min.js"></script>
    <script src="<?php echo base_url()?>public/asset/plugins/jquery-mapael/maps/usa_states.min.js"></script>
    <!-- ChartJS -->
    <script src="<?php echo base_url()?>public/asset/plugins/chart.js/Chart.min.js"></script>

    <!-- PAGE SCRIPTS -->
    <!-- <script src="<?php // echo base_url()?>public/asset/dist/js/pages/dashboard2.js"></script> -->
    <script src="<?php echo base_url()?>public/asset/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>public/asset/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script src="<?php echo base_url()?>public/asset/plugins/toastr/toastr.min.js"></script>
    <script src="<?php echo base_url()?>public/asset/plugins/swal/sweetalert2.min.js"></script>

    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h4>Data Kasus Covid-19 Seluruh Dunia &nbsp;
                </h4>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                <table id="mydata" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td><b>No.</b></td>
                            <td><b>Negara</b></td>
                            <td><b>Positif</b></td>
                            <td><b>Sembuh</b></td>
                            <td><b>Meninggal</b></td>
                            <td><b>% Deaths</b></td>
                            <td><b>Last Update</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $no = 1;
                        foreach ($resultworld as $row) {
                    ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->attributes->Country_Region; ?></td>
                            <td><?php echo number_format($row->attributes->Confirmed,0,",",","); ?></td>
                            <td><?php echo number_format($row->attributes->Recovered,0,",",","); ?></td>
                            <td><?php echo number_format($row->attributes->Deaths,0,",",","); ?></td>
                            <?php
                                $meninggal = $row->attributes->Deaths;
                                $positif = $row->attributes->Confirmed;
                                $persen = ($meninggal / $positif) * 100;
                            ?>
                            <td><?php echo round($persen,2); ?> %</td>
                            <td><?php echo substr($row->attributes->Last_Update,4,2).":".substr($row->attributes->Last_Update,6,2).":".substr($row->attributes->Last_Update,8,2) ?></td>
                        </tr>
                    <?php $no++; } ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <h4>Data Kasus Covid-19 Provinsi di Indonesia &nbsp;
                </h4>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <div class="table-responsive">
                <table id="mydata2" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <td><b>No.</b></td>
                            <td><b>Provinsi</b></td>
                            <td><b>Positif</b></td>
                            <td><b>Sembuh</b></td>
                            <td><b>Meninggal</b></td>
                            <td><b>% Deaths</b></td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $no = 1;
                        foreach ($resultidn as $row) {
                    ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row->attributes->Provinsi; ?></td>
                            <td><?php echo $row->attributes->Kasus_Posi; ?></td>
                            <td><?php echo $row->attributes->Kasus_Semb; ?></td>
                            <td><?php echo $row->attributes->Kasus_Meni; ?></td>
                            <?php
                                $meninggal = $row->attributes->Kasus_Meni;
                                $positif = $row->attributes->Kasus_Posi;
                                $persen = ($meninggal / $positif) * 100;
                            ?>
                            <td><?php echo round($persen,2); ?> %</td>
                        </tr>
                    <?php $no++; } ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>

    
    <footer>
        <div class="alert alert-light" role="alert">
            <center>Copyright : A. Fajar Prakoso</center> 
        </div>
    </footer>
</body>

</html>
<script>
    $(document).ready(function() {
        $('#mydata').DataTable({
            "scrollY": "450px",
            "scrollCollapse": true,
        });
        $('#mydata2').DataTable({
            "scrollY": "450px",
            "scrollCollapse": true,
        });
    } );
</script>